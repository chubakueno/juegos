#include <set>
#include <iostream>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

typedef websocketpp::server<websocketpp::config::asio> server;

using websocketpp::connection_hdl;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;
using namespace std;
float posx[1000];
float posy[1000];
int  killedby[1000];
class broadcast_server {
public:
	broadcast_server() {
		memset(posx, 0, sizeof posx);
		memset(posy, 0, sizeof posy);
		memset(killedby, -1, sizeof killedby);
		m_server.init_asio();

		m_server.set_open_handler(bind(&broadcast_server::on_open, this, ::_1));
		m_server.set_close_handler(bind(&broadcast_server::on_close, this, ::_1));
		m_server.set_message_handler(bind(&broadcast_server::on_message, this, ::_1, ::_2));
	}

	void on_open(connection_hdl hdl) {
		m_connections[hdl]=m_connections.size();
		std::stringstream ss;
		int id = m_connections[hdl];
		ss << id<<" C";
		m_server.send(hdl, ss.str(), websocketpp::frame::opcode::text);
		for (int i = 0; i < 1000; ++i) {
			if (posx[i]||posy[i]) {
				stringstream buf;
				buf << i << " P " << posx[i] << " " << posy[i];
				m_server.send(hdl, buf.str(), websocketpp::frame::opcode::text);
			}
			if (killedby[i]>=0) {
				stringstream buf;
				buf << killedby[i] << " I " << i;
				m_server.send(hdl, buf.str(), websocketpp::frame::opcode::text);
			}
		}
	}

	void on_close(connection_hdl hdl) {
		m_connections.erase(hdl);
	}

	void on_message(connection_hdl hdl, server::message_ptr msg) {
		string res = msg->get_payload();
		if (res.length() == 0) return;
		stringstream ss(res);
		int id;
		string op;
		ss >> id >> op;
		if (op == "P") {
			float x, y;
			ss >> x >> y;
			posx[id] = x;
			posy[id] = y;
		}
		if (op == "I") {
			int killed;
			ss >> killed;
			killedby[killed] = id;
		}
		std::cout << msg->get_payload()<< std::endl;
		for (auto it : m_connections) {
			if (m_connections[it.first] == m_connections[hdl]) continue;
			m_server.send(it.first, msg);
		}
	}

	void run(uint16_t port) {
		m_server.listen(port);
		m_server.start_accept();
		m_server.run();
	}
private:
	typedef std::map<connection_hdl,int, std::owner_less<connection_hdl>> con_list;

	server m_server;
	con_list m_connections;
};

int main() {
	broadcast_server server;
	server.run(9002);
}