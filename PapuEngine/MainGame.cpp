#include "MainGame.h"
#include "ImageLoader.h"
#include <iostream>
#include "ResourceManager.h"
#include "PapuEngine.h"
#include <random>
#include <ctime>
#include "networking.h"

using namespace std;

void MainGame::run(int id) {
	_id = id;
	init();
	update();
}

void MainGame::init() {
	Papu::init();
	_window.create("Engine", _witdh, _height, 0);
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	initLevel();
	initShaders();
}

void MainGame::initLevel() {
	_levels.push_back(new Level("Levels/level1.txt"));
	_currenLevel = 0;
	_spriteBacth.init();

	/*std::mt19937 randomEngine(time(nullptr));
	std::uniform_int_distribution<int>randPosX(
		1, _levels[_currenLevel]->getWidth()-2);
	std::uniform_int_distribution<int>randPosY(
		1, _levels[_currenLevel]->getHeight()-2);

	for (size_t i = 0; i < _levels[_currenLevel]->getNumHumans(); i++)
	{
		_humans.push_back(new Human());
		glm::vec2 pos(randPosX(randomEngine)*TILE_WIDTH, 
							randPosY(randomEngine)*TILE_WIDTH);
		_humans.back()->init(1.0f, pos);
	}*/

	const std::vector<glm::vec2>& zombiePosition =
		_levels[_currenLevel]->getZombiesPosition();

	for (size_t i = 0; i < zombiePosition.size(); i++)
	{
		_zombies.push_back(new Player());
		_zombies.back()->init(60*(i<8?1.0:0.8), zombiePosition[i]);
	}
	_player = _zombies[_id];
}

void MainGame::initShaders() {
	_program.compileShaders("Shaders/colorShaderVert.txt",
		"Shaders/colorShaderFrag.txt");
	_program.addAtribute("vertexPosition");
	_program.addAtribute("vertexColor");
	_program.addAtribute("vertexUV");
	_program.linkShader();
}


void MainGame::draw() {
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_program.use();

	glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, _texture.id);

	/*GLuint timeLocation = 
		_program.getUniformLocation("time");

	glUniform1f(timeLocation,_time);*/

	GLuint pLocation =
		_program.getUniformLocation("P");

	glm::mat4 cameraMatrix = _camera.getCameraMatrix();
	glUniformMatrix4fv(pLocation, 1,GL_FALSE, &(cameraMatrix[0][0]));

	GLuint imageLocation = _program.getUniformLocation("myImage");
	glUniform1i(imageLocation, 0);

	_spriteBacth.begin();
	_levels[_currenLevel]->draw();
	for (size_t i = 0; i < _zombies.size(); i++)
	{
		if (!_zombies[i]->getActive()) continue;
		_zombies[i]->draw(_spriteBacth);
	}

	_spriteBacth.end();
	_spriteBacth.renderBatch();

	glBindTexture(GL_TEXTURE_2D, 0);

	_program.unuse();
	_window.swapBuffer();
}

void MainGame::procesInput() {
	SDL_Event event;
	const float CAMERA_SPEED = 20.0f;
	const float SCALE_SPEED = 0.1f;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				_gameState = GameState::EXIT;
				break;
			case SDL_MOUSEMOTION:
				_inputManager.setMouseCoords(event.motion.x,event.motion.y);
			break;
			case  SDL_KEYUP:
				_inputManager.releaseKey(event.key.keysym.sym);
				break;
			case  SDL_KEYDOWN:
				_inputManager.pressKey(event.key.keysym.sym);
				break;
			case SDL_MOUSEBUTTONDOWN:
				_inputManager.pressKey(event.button.button);
				break;
			case SDL_MOUSEBUTTONUP:
				_inputManager.releaseKey(event.button.button);
				break;
		}

		/*if (_inputManager.isKeyPressed(SDLK_w)) {
			_camera.setPosition(_camera.getPosition() + glm::vec2(0.0, CAMERA_SPEED));
		}
		if (_inputManager.isKeyPressed(SDLK_s)) {
			_camera.setPosition(_camera.getPosition() + glm::vec2(0.0, -CAMERA_SPEED));
		}
		if (_inputManager.isKeyPressed(SDLK_a)) {
			_camera.setPosition(_camera.getPosition() + glm::vec2(-CAMERA_SPEED, 0.0));
		}
		if (_inputManager.isKeyPressed(SDLK_d)) {
			_camera.setPosition(_camera.getPosition() + glm::vec2(CAMERA_SPEED, 0.0));
		}*/
		if (_inputManager.isKeyPressed(SDLK_q)) {
			_camera.setScale(_camera.getScale() + SCALE_SPEED);
		}
		if (_inputManager.isKeyPressed(SDLK_e)) {
			_camera.setScale(_camera.getScale() - SCALE_SPEED);
		}
		if (_inputManager.isKeyPressed(SDL_BUTTON_LEFT)) {
			glm::vec2 mouseCoords =  _camera.convertScreenToWorl(_inputManager.getMouseCoords());
			cout << mouseCoords.x << " " << mouseCoords.y << endl;
			glm::vec2 playerPosition(0, 0);

			glm::vec2 direction = mouseCoords - playerPosition;
			direction = glm::normalize(direction);
			//_bullets.emplace_back(playerPosition, direction, 1.0f,1000);
		}
	}
}
void MainGame::updateFromNetwork() {
	while (true) {
		string res = readMessage();
		if (res.length() == 0) return;
		stringstream ss(res);
		int id;
		string op;
		ss >> id >> op;
		if (op == "C") continue;
		if (op == "P") {
			float x, y;
			ss >> x >> y;
			_zombies[id]->setPosition(glm::vec2(x, y));
		}
		if (op == "I") {
			int killed;
			ss >> killed;
			_zombies[id]->setDiameter(_zombies[id]->getDiameter() + 10);
			_zombies[killed]->setInactive();
		}
	}
}
void MainGame::updateFromLocal() {
	if (!_player) return;
	float speed = 1.0;
	glm::vec2 position = _player->getPosition();
	if (_inputManager.isKeyPressed(SDLK_w)) {
		position.y += speed*timeDelta;
	}
	if (_inputManager.isKeyPressed(SDLK_s)) {
		position.y -= speed*timeDelta;
	}
	if (_inputManager.isKeyPressed(SDLK_a)) {
		position.x -= speed*timeDelta;
	}
	if (_inputManager.isKeyPressed(SDLK_d)) {
		position.x += speed*timeDelta;
	}
	_player->setPosition(position);
}
void MainGame::update() {
	while (_gameState != GameState::EXIT) {
		if (_player && !_player->getActive()) {
			break;
		}
		glm::vec2 pos1, pos2;
		if (_player) pos1 = _player->getPosition();
		updateFromNetwork();
		updateFromLocal();
		previous = current;
		QueryPerformanceCounter((LARGE_INTEGER*)&current);
		timeDelta = (current - previous)*0.0005;
		procesInput();
		updateAgents();
		if(_player) _camera.setPosition(_player->getPosition());
		_camera.update();
		_time += 0.002f;
		draw();
		if (_player) {
			pos2 = _player->getPosition();
			if (pos1 != pos2) {
				stringstream ss;
				ss << "P " << pos2.x << " " << pos2.y;
				sendMessage(ss.str());
			}
		}
	}
}

void MainGame::updateAgents() {
	if (!_player) return;
	if (!_player->getActive()) {
		//SE MURIOOOOO
	}
	_player->update(_levels[_currenLevel]->getLevelData(), timeDelta);
	for (int i=0;i<_zombies.size();++i)
	{
		Player* zombie = _zombies[i];
		if (zombie->getActive()&&_player->getDiameter() > zombie->getDiameter()&& zombie->collideWithAgent(_player)) {
			_player->setDiameter(_player->getDiameter() + 10);
			zombie->setInactive();
			stringstream ss;
			ss << "I " << i;
			sendMessage(ss.str());
		}
	}
}

MainGame::MainGame(): 
					  _witdh(800),
					  _height(600),
					  _gameState(GameState::PLAY),
					  _time(0),
					  _player(nullptr)
{
	QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);
	QueryPerformanceCounter((LARGE_INTEGER*)&current);
	_camera.init(_witdh, _height);
}


MainGame::~MainGame()
{
}
