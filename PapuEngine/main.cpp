#include <SDL\SDL.h>
#include "MainGame.h"
#include "networking.h"
#include "App.h"
#include <boost\thread.hpp>
void task()
{
	listen();
}
int main(int argc, char** argv) {
	SDL_Init(SDL_INIT_EVERYTHING);
	boost::thread t1{task};
	string str = blockingReadMessage();
	stringstream ss;
	ss << str;
	int id;
	ss >> id;
	setId(id);
	cout << id << endl;
	/*MainGame mainge;
	mainge.run(id);*/
	App app;
	app.run();
	close_connection();
	t1.join();
	return 0;
}