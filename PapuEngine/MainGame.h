#pragma once
#include <SDL\SDL.h>
#include <GL\glew.h>
#include "GLS_Program.h"
#include "Window.h"
#include "Camera2D.h"
#include <vector>
#include "SpriteBacth.h"
#include "InputManager.h"
#include "Bullet.h"
#include "Level.h"
#include "Player.h"
#include "Human.h"
#include "Zombie.h"

enum class GameState
{
	PLAY, EXIT
};


class MainGame
{
private:
	int _witdh;
	int _height;
	float _time;
	int _id;
	Window _window;
	void init();
	void procesInput();
	void updateFromNetwork();
	void updateFromLocal();
	GLS_Program _program;
	Camera2D _camera;
	SpriteBacth _spriteBacth;
	InputManager _inputManager;
	vector<Bullet> _bullets;
	vector<Level*> _levels;
	vector<Player*> _zombies;
	Player* _player=nullptr;
	int _currenLevel;
	void initLevel();
	void updateAgents();
	long long previous;
	long long current;
	long long frequency;
	double timeDelta;
public:
	MainGame();
	~MainGame();
	GameState _gameState;
	void initShaders();
	void run(int);
	void draw();
	void update();
};

