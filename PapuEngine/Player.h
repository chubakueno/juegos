#pragma once
#include "Human.h"
#include "InputManager.h"

class Player: public Human
{
public:
	Player();
	~Player();
	void init(float speed, glm::vec2 position);
	void update(const std::vector<std::string>& levelData, float timeDelta);
};

